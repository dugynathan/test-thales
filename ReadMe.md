## Test Thales ASCII-ART

Ce code a pour but d'afficher un ASCII-ART sur un terminal linux à partir de paramètres fournis.

# Exécuter

- gcc -o canvas canvas.c
- ./canvas

# Exemples

Voir Résultats_sur_Terminal.txt
