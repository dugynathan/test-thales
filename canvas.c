#include <stdio.h>
#include <stdlib.h>

#define NB_CHARS 27
#define BUFFER_SIZE 512

int main(int argc, char *argv[])
{
  int w_size = 0; /* permet d'identifier la taille du mot à réécrire en ASCII ART */
  int height;
  int widths[2][NB_CHARS]; /* On stocke l'ensemble des largeurs dans une matrice, la première ligne correspondant à la largeur de la lettre et la seconde la valeur d'arrêt de la lettre (exemple si A=4 et B=4, alors A termine à 4 et B à 8) */
  int word[BUFFER_SIZE]; /* On stocke l'ensemble des id des lettres composants le mot */
  static char line[BUFFER_SIZE];

  /* char height */
  scanf("%d", &height); /* J'ai moins de soucis à utilise scanf que fgets */
  fgetc(stdin);
    
  char ascii_art[height][BUFFER_SIZE]; /* On stocke les ASCII ART à respecter dans une matrice */

  /* On stocke les valeurs de largeur dans widths */
  for (int i = 0; i < NB_CHARS; i++) {
      scanf("%d", &widths[0][i]); /* la largeur de la ieme lettre dans widths[0] */
      if (i != 0) {
          /* la valeur de fin de la ieme lettre dans widths[0] dans l'ASCII (permet d'éviter des calculs par la suite */
          widths[1][i] = widths[1][i-1] + widths[0][i];
      } else {
          widths[1][i] = widths[0][i]; /* cas de A */
      }
  }

  /* text to print */
    scanf("%s", &line);

  /* On stocke le texte */
  while (line[w_size] != '\0') {
      /* On utilise les valeurs ASCII directement, ça facilite grandement les comparaisons entre caractères */
      if (line[w_size] >= 'a' && line[w_size] <= 'z') {
          /* les valeurs ASCII de l'alphabet se suivant a permet de retrouver l'ID de la lettre dans l'alphabet (exemple b = a+1 donc b-a = 1) */
          word[w_size] = line[w_size] - 'a';
      } else if (line[w_size] >= 'A' && line[w_size] <= 'Z') {
          /* idem */
          word[w_size] = line[w_size] - 'A';
      } else {
          word[w_size] = NB_CHARS - 1; /* Cas du ? */
      }
      w_size++;
  }

  /* Tampon, si j'enlève cette ligne, le code ne fonctionne plus correctement */
  fgets(line, BUFFER_SIZE, stdin);

  /* ASCII ART to print : On stocke chacune des lignes dans la matrice ascii_art */
  for (int i = 0; i < height; i++) {
    fgets(line, BUFFER_SIZE, stdin);
    for (int j = 0; line[j] != '\0'; j++) {
      ascii_art[i][j] = line[j];
    }
  }

  /* final print */
  for (int n_line = 0; n_line < height; n_line++) {
    for (int i = 0; i < w_size; i++) {
      for (int j = 0; j < widths[0][word[i]]; j++) {
        if (word[i] != 0){
          /* On utilise l'ensemble des variables pré-établies pour retrouver les éléments à afficher */
          printf("%c", ascii_art[n_line][widths[1][word[i]-1] + j]);
        } else {
          printf("%c", ascii_art[n_line][j]); /* Cas du A */
        }
      }
    }
    printf("\n");
  }

  return 0;
}
